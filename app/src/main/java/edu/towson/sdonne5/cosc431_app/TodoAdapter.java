package edu.towson.sdonne5.cosc431_app;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import edu.towson.sdonne5.cosc431_app.models.Todo;
import edu.towson.sdonne5.cosc431_app.networking.DownloadImageTask;
import edu.towson.sdonne5.cosc431_app.networking.TodoFetcherTask;
import edu.towson.sdonne5.cosc431_app.presenters.TodoPresenter;

/**
 * @author Sean on 3/6/2017.
 */

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoHolder> {

    private List<Todo> todos;
    private LayoutInflater inflater;
    public TodoPresenter presenter;

    public TodoAdapter(List<Todo> listData, Context c, TodoPresenter presenter){
        inflater = LayoutInflater.from(c);
        this.todos = listData;
        this.presenter = presenter;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.todo_item, parent, false);
        return new TodoHolder(view, presenter);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        Todo todo = todos.get(position);
        holder.title.setText(todo.getTitle());
        holder.contents.setText(todo.getContents());
        holder.important.setChecked(todo.isImportant());
        holder.complete.setChecked(todo.isComplete());
        holder.dateCreated.setText(todo.getDateCreated().toString());
        //Sets the due date text to the date if it exists
        String dueDateText;
        if(todo.getDueDate() != null)
            dueDateText = todo.getDueDate().toString();
        else dueDateText = "No Due Date Selected";
        holder.dateDue.setText(dueDateText);
    }

    @Override
    public int getItemCount() {
        return todos.size();
    }

    public class TodoHolder extends RecyclerView.ViewHolder {

        public TextView title, contents, dateCreated, dateDue;
        private Switch complete, important;
        private View container;
        private TodoPresenter presenter;

        public TodoHolder(View itemView, TodoPresenter presenter) {
            super(itemView);
            bindView();
            setListeners();
            this.presenter = presenter;
        }
        private void bindView() {
            title = (TextView)itemView.findViewById(R.id.todoTitle);
            contents = (TextView)itemView.findViewById(R.id.todoContent);
            dateCreated = ((TextView)itemView.findViewById(R.id.todoDateCreated));
            dateDue = ((TextView)itemView.findViewById(R.id.todoDueDate));
            complete = ((Switch)itemView.findViewById(R.id.important_toggle));
            important = ((Switch)itemView.findViewById(R.id.important_toggle));
            container = itemView.findViewById(R.id.todo_container);
            new DownloadImageTask((ImageView) itemView.findViewById(R.id.imageView))
                    .execute("https://api.adorable.io/avatars/285/abott@adorable.png");
        }
        private void setListeners() {
            itemView.findViewById(R.id.todo_container).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    //TODO trigger edit event from here
                    int index = TodoHolder.super.getAdapterPosition();
                        if(presenter != null) {
                            presenter.editTodo(index);
                        }
                }
            });
            itemView.findViewById(R.id.todo_container).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int index = TodoHolder.super.getAdapterPosition();
                    presenter.deleteTodo(presenter.getTodos().get(index).getId());
                    TodoAdapter.super.notifyItemRemoved(index);
                    todos.remove(index);
                    return true;
                }
            });
            //Sets the onClick listener for the important toggle switch
            ((Switch)itemView.findViewById(R.id.important_toggle)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                        ((TextView)TodoHolder.super.itemView.findViewById(R.id.todoTitle)).setTextColor(Color.RED);
                    else ((TextView)TodoHolder.super.itemView.findViewById(R.id.todoTitle)).setTextColor(Color.BLACK);
                }
            });
            //Sets the onClick listener for the completed toggle switch
            ((Switch)itemView.findViewById(R.id.completed_toggle)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                        ((TextView)TodoHolder.super.itemView.findViewById(R.id.todoTitle)).setTextColor(Color.GREEN);
                    else ((TextView)TodoHolder.super.itemView.findViewById(R.id.todoTitle)).setTextColor(Color.BLACK);
                }
            });
        }
    }
}
