package edu.towson.sdonne5.cosc431_app.interfaces.events;

/**
 * @author Sean on 3/27/2017.
 */

public interface IEventListener {
    // listeners implement this method to receive events
    void onEvent(IEventEmitter.IEvent event);
}
