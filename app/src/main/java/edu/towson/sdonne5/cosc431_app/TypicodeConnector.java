package edu.towson.sdonne5.cosc431_app;

import java.util.ArrayList;

import edu.towson.sdonne5.cosc431_app.interfaces.IDataSource;
import edu.towson.sdonne5.cosc431_app.models.Todo;
import edu.towson.sdonne5.cosc431_app.networking.TodoFetcherTask;

/**
 * @author Sean on 5/1/2017.
 */

public class TypicodeConnector implements IDataSource {
    ArrayList<Todo> list;
    public TypicodeConnector(TodoActivity activity) {
        list = new ArrayList<>();
        new TodoFetcherTask(this, activity).execute("https://jsonplaceholder.typicode.com/todos?userId=1");
    }
    public void setTodos(ArrayList<Todo> l) {
        this.list = l;
    }
    @Override
    public ArrayList<Todo> getTodos() {
       // System.out.println(list.get(0).getTitle());
        return list;
    }

    @Override
    public void createTodo(String title, String contents) {

    }

    @Override
    public void updateTodo(Todo t) {

    }

    @Override
    public void deleteTodo(int id) {

    }
}
