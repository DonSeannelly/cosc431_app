package edu.towson.sdonne5.cosc431_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;

/**
 * @author Sean on 2/19/2017.
 */
public class CreateTodoActivity extends AppCompatActivity {

    private EditText createTodoTitle, createTodoContents, createTodoDate;
    private CheckBox createTodoImportant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_todo);

        bindView();

        //TODO if the user intends to edit, fetch task from intent's bundle
        Intent intent = getIntent();
    }

    private void bindView() {
        createTodoTitle = (EditText) findViewById(R.id.createTodoTitle);
        createTodoContents = (EditText) findViewById(R.id.createTodoContents);
        createTodoDate = (EditText) findViewById(R.id.createTodoDate);
        createTodoImportant = (CheckBox) findViewById(R.id.createTodoImportant);
    }
    void createTodo(View v) {
        Intent intent = new Intent(this, TodoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("CREATE_TODO_TITLE", createTodoTitle.getText().toString());
        bundle.putString("CREATE_TODO_CONTENTS", createTodoContents.getText().toString());
        bundle.putString("CREATE_TODO_DATE", createTodoDate.getText().toString());
        bundle.putBoolean("CREATE_TODO_IMPORTANT", createTodoImportant.isEnabled());
        intent.putExtra("CREATED_TODO", bundle);
        startActivity(intent);
    }
}
