package edu.towson.sdonne5.cosc431_app.models;

import java.util.Date;

/**
 * Created by smdon on 2/9/2017.
 */
public class Todo {
    public int getId() {
        return id;
    }

    private int id;
    private String title;
    private String contents;
    private boolean complete, important;
    private Date created, due;

    public Todo(int id, String title, String contents, boolean complete, boolean important) {
        this.id = id;
        this.title = title;
        this.contents = contents;
        this.complete = complete;
        this.important = important;
        this.created = new Date();
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContents() {
        return contents;
    }
    public void setContents(String contents) {
        this.contents = contents;
    }
    public void toggleComplete() { this.complete = !this.complete; }
    public void toggleimportant() { this.important = !this.important; }
    public void setDueDate(Date date) { this.due = date; }
    public Date getDueDate() { return due; }
    public Date getDateCreated() { return created; }
    public boolean isImportant() {
        return important;
    }

    public boolean isComplete() {
        return complete;
    }

}
