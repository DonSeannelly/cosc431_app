package edu.towson.sdonne5.cosc431_app.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import edu.towson.sdonne5.cosc431_app.R;
import edu.towson.sdonne5.cosc431_app.presenters.TodoPresenter;

/**
 * @author Sean on 3/27/2017.
 */

public class MainView extends LinearLayout {
    TodoPresenter presenter;

    public MainView(Context context) {
        super(context);
        bindView(context);
    }

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindView(context);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindView(context);
    }

    private void bindView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.main_view, this);
    }
}
