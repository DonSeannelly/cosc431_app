package edu.towson.sdonne5.cosc431_app.presenters;

import edu.towson.sdonne5.cosc431_app.interfaces.events.IEventEmitter;
import edu.towson.sdonne5.cosc431_app.interfaces.events.IEventListener;

/**
 * @author Sean on 3/27/2017.
 */


// This presenter manages the communication between 2 presenters
// It is an event listener, it subscribes to the 2 presenters and listens to their events
public class SharedPresenter implements IEventListener {

    private TodoPresenter todoPresenter;
    private EditPresenter editPresenter;

    public SharedPresenter(TodoPresenter presenter, EditPresenter editPresenter) {
        this.editPresenter = editPresenter;
        this.todoPresenter = presenter;
    }

    /**
     * This is the EventListener interface. IEvent has the event type and the value of the event
     * @param event
     */
    @Override
    public void onEvent(IEventEmitter.IEvent event) {
        // if the event type is the EDIT event from the main presenter,
        // then set the model on the edit presenter.
        if(event.getType().equals(TodoPresenter.Events.EDIT)) {
            editPresenter.setModel(event.getTodo());
        }
        // if the event type is the SAVE event from the edit presenter
        // then set the model on the main presenter.
        if(event.getType().equals(EditPresenter.Events.SAVE)) {
            todoPresenter.setModel(event.getTodo());
        }
    }
}