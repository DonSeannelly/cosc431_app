package edu.towson.sdonne5.cosc431_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import edu.towson.sdonne5.cosc431_app.interfaces.IDataSource;
import edu.towson.sdonne5.cosc431_app.presenters.EditPresenter;
import edu.towson.sdonne5.cosc431_app.presenters.SharedPresenter;
import edu.towson.sdonne5.cosc431_app.presenters.TodoPresenter;
import edu.towson.sdonne5.cosc431_app.views.EditView;

/**
 * View for the Todos
 */
public class TodoActivity extends AppCompatActivity {

    private TodoPresenter presenter;
    private RecyclerView recyclerView;
    private TodoAdapter adapter;
    EditPresenter editPresenter;
    SharedPresenter sharedPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        IDataSource dbHelper = new TypicodeConnector(this);
        //Attaches this view to the presenter
        if(presenter == null)
            presenter = new TodoPresenter(dbHelper);

        checkForIntent();
        bindView();
        presenter.attachView(this);

        // this might be null (if in portrait)
        EditView editView = (EditView)findViewById(R.id.editView);

        // only if the edit view is not null do we create a presenter for it
        if(editView != null) {
            editPresenter = new EditPresenter(editView, dbHelper);
            editView.setPresenter(editPresenter);

            // use a shared presenter to manage the communication between views/presenters
            sharedPresenter = new SharedPresenter(presenter, editPresenter);
            // the presenters can be subscribed to (which means that they emit events)
            // the shared presenter listens to these events and delegates them to the appropriate other presenter
            presenter.subscribe(sharedPresenter);
            editPresenter.subscribe(sharedPresenter);
        }

    }
    /**
     * Checks if the intent contains a bundle with a new task
     */
    public void checkForIntent() {
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("CREATED_TODO");
        if(bundle != null) {
            presenter.addTodo(bundle.getString("CREATE_TODO_TITLE"),
                    bundle.getString("CREATE_TODO_CONTENTS"),
                    bundle.getBoolean("CREATE_TODO_IMPORTANT"),
                    bundle.getString("CREATE_TODO_DATE"));
        }
        getIntent().removeExtra("CREATED_TODO");
    }
    public void bindView() {
        recyclerView = (RecyclerView)findViewById(R.id.rec_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TodoAdapter(presenter.getTodos(), this, presenter);
        recyclerView.setAdapter(adapter);

    }
    public void launchCreateTodoActivity(View v) {
        Intent intent = new Intent(this, CreateTodoActivity.class);
        startActivity(intent);
    }
}
