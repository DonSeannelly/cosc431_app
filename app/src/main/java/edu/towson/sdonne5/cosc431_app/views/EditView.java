package edu.towson.sdonne5.cosc431_app.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import edu.towson.sdonne5.cosc431_app.R;
import edu.towson.sdonne5.cosc431_app.models.Todo;
import edu.towson.sdonne5.cosc431_app.presenters.EditPresenter;

/**
 * @author Sean on 3/27/2017.
 */

public class EditView extends LinearLayout {
    EditText title, contents;

    EditPresenter presenter;

    public EditView(Context context) {
        super(context);
        bindView(context);
    }

    public EditView(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindView(context);
    }

    public EditView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindView(context);
    }

    private void bindView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.edit_view, this);
        title = (EditText)findViewById(R.id.todoTitle);
        contents = (EditText)findViewById(R.id.todoContents);
        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onModelEdited(title.getText().toString(), contents.getText().toString());
                presenter.save();
                Toast.makeText(EditView.super.getContext(), "Task Updated", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void clear() {
        title.setText("");
        contents.setText("");
    }

    public void sendToMainActivity(String s) {
        Intent intent = new Intent();
        intent.putExtra("TEXT", s);
        ((Activity)getContext()).setResult(Activity.RESULT_OK, intent);
        ((Activity)getContext()).finish();
    }

    public void setModel(Todo t) {
        title.setText(t.getTitle());
        contents.setText(t.getContents());

    }

    public void setPresenter(EditPresenter presenter) {
        this.presenter = presenter;
    }

}
