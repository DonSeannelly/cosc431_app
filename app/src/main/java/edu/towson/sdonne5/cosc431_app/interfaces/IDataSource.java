package edu.towson.sdonne5.cosc431_app.interfaces;

import java.util.ArrayList;

import edu.towson.sdonne5.cosc431_app.models.Todo;

/**
 * @author Sean on 3/6/2017.
 */

public interface IDataSource {
    ArrayList<Todo> getTodos();
    void createTodo(String title, String contents);
    void updateTodo(Todo t);
    void deleteTodo(int id);
}
