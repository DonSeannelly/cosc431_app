package edu.towson.sdonne5.cosc431_app.interfaces.events;

import edu.towson.sdonne5.cosc431_app.models.Todo;

/**
 * @author Sean on 3/27/2017.
 */

public interface IEventEmitter {
    // subscribe to events from this event emitter
    void subscribe(IEventListener event);

    // the type of events that are emitted
    interface IEvent<T> {
        T getType();
        Todo getTodo();
    }
}
