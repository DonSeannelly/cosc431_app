package edu.towson.sdonne5.cosc431_app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import edu.towson.sdonne5.cosc431_app.interfaces.IDataSource;
import edu.towson.sdonne5.cosc431_app.models.Todo;

/**
 * @author Sean on 3/27/2017.
 */

public class DBHelper extends SQLiteOpenHelper implements IDataSource {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "cosc431_db";
    private static final String TABLE_TODOS = "todos";
    // Todos Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_CONTENTS = "contents";

    
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_TODOS + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY," +
                    KEY_TITLE + " TEXT," +
                    KEY_CONTENTS + " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_TODOS;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onCreate(sqLiteDatabase);
    }

    public void createTodo(String title, String contents) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, title);
        values.put(KEY_CONTENTS, contents);
        db.insert(TABLE_TODOS, null, values);
        db.close(); // Closing database connection
    }
    @Override
    public ArrayList<Todo> getTodos() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                KEY_ID,
                KEY_TITLE,
                KEY_CONTENTS
        };
        Cursor cursor = db.query(
                TABLE_TODOS,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        ArrayList todos = new ArrayList<>();
        while(cursor.moveToNext()) {
            todos.add(new Todo(cursor.getInt(0),cursor.getString(1),cursor.getString(2),false,false));
            System.out.println("#" + cursor.getInt(0) + " - " + cursor.getString(1));
        }
        cursor.close();
        return todos;
    }

    public void updateTodo(Todo updated) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, updated.getTitle());
        values.put(KEY_CONTENTS, updated.getContents());
        // Which row to update, based on the id
/*        String selection = KEY_ID + " ?";
        String[] selectionArgs = { Integer.toString(updated.getId()) };

        int count = db.update(TABLE_TODOS,values,selection,selectionArgs);*/
        db.execSQL("UPDATE " + TABLE_TODOS
                + " SET " + KEY_TITLE + " = \'" + updated.getTitle() + "\', "
                + KEY_CONTENTS + " = \'" + updated.getContents() +
        "\' WHERE " + KEY_ID + " = " + updated.getId());
    }

    public void deleteTodo(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TODOS + " WHERE id = " + id);
    }
}
