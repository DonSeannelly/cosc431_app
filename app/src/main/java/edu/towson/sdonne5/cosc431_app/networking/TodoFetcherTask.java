package edu.towson.sdonne5.cosc431_app.networking;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import edu.towson.sdonne5.cosc431_app.TodoActivity;
import edu.towson.sdonne5.cosc431_app.TypicodeConnector;
import edu.towson.sdonne5.cosc431_app.models.Todo;

/**
 * @author Sean on 5/1/2017.
 */

public class TodoFetcherTask extends AsyncTask<String, String, String> {
    TypicodeConnector c;
    TodoActivity activity;
    public TodoFetcherTask(TypicodeConnector c, TodoActivity activity) {
        this.c = c;
        this.activity = activity;
    }
    protected String doInBackground(String... params) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
                Log.d("Response: ", "> " + line);
            }
            return buffer.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        ArrayList<Todo> list = new ArrayList<>();
        try {
            JSONArray resultArray = new JSONArray(result);
            for(int i = 0; i < resultArray.length(); i++) {
                list.add(new Todo(
                        resultArray.optJSONObject(i).getInt("id"),
                        resultArray.optJSONObject(i).getString("title"),
                        "",
                        resultArray.optJSONObject(0).getBoolean("completed"),
                        false
                ));
            }
            c.setTodos(list);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        activity.bindView();
    }
}
