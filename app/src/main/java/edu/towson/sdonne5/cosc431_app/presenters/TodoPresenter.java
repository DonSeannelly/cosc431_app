package edu.towson.sdonne5.cosc431_app.presenters;

import java.util.ArrayList;
import java.util.List;

import edu.towson.sdonne5.cosc431_app.models.Todo;
import edu.towson.sdonne5.cosc431_app.TodoActivity;
import edu.towson.sdonne5.cosc431_app.interfaces.IDataSource;
import edu.towson.sdonne5.cosc431_app.interfaces.events.IEventEmitter;
import edu.towson.sdonne5.cosc431_app.interfaces.events.IEventListener;

/**
 * Created by smdon on 2/9/2017.
 */
public class TodoPresenter {

    enum Events {
        EDIT
    }

    private TodoActivity activity;
    private IDataSource dataSource;
    private List<IEventListener> listeners;
    private int currentEditIndex;

    public TodoPresenter(IDataSource dataSource) {
        this.dataSource = dataSource;
        listeners = new ArrayList<>();
    }
    public void attachView(TodoActivity activity) {
        this.activity = activity;
    }
    public List<Todo> getTodos() {
        return dataSource.getTodos();
    }
    public void addTodo(String title, String contents, boolean important, String dateDue) {
        //TODO Add important and due date functionality
        dataSource.createTodo(title, contents);
    }


    public void editTodo(int index) {
        currentEditIndex = index;
        if(listeners.isEmpty()) {
            // if there are no listeners (no shared presenter), launch a new activity
            //TODO add edit activity
        } else {
            // otherwise, emit the EDIT event
            for (IEventListener listener : listeners) {
                listener.onEvent(new Event(Events.EDIT, dataSource.getTodos().get(index)));
            }
        }
    }

    public void deleteTodo(int index) {
        dataSource.deleteTodo(index);
    }

    public void setModel(Todo t) {
        activity.bindView();
    }

    public void subscribe(IEventListener event) {
        listeners.add(event);
    }

    /**
     * The event object that this presenter emits
     */
    private class Event implements IEventEmitter.IEvent<TodoPresenter.Events> {

        TodoPresenter.Events type;
        Todo todo;
        Event(TodoPresenter.Events type, Todo todo){
            this.type = type;
            this.todo = todo;
        }

        @Override
        public TodoPresenter.Events getType() {
            return type;
        }

        @Override
        public Todo getTodo() {
            return todo;
        }
    }
}
