package edu.towson.sdonne5.cosc431_app.presenters;

import java.util.ArrayList;
import java.util.List;

import edu.towson.sdonne5.cosc431_app.models.Todo;
import edu.towson.sdonne5.cosc431_app.interfaces.IDataSource;
import edu.towson.sdonne5.cosc431_app.interfaces.events.IEventEmitter;
import edu.towson.sdonne5.cosc431_app.interfaces.events.IEventListener;
import edu.towson.sdonne5.cosc431_app.views.EditView;

/**
 * @author Sean on 3/27/2017.
 */

public class EditPresenter {

    enum Events {
        SAVE
    }
    private EditView view;
    private Todo model;
    private List<IEventListener> listeners;
    private IDataSource dbhelper;

    public EditPresenter(EditView view, IDataSource dbhelper) {
        this.view = view;
        this.model = new Todo(-1,"","",false,false);
        listeners = new ArrayList<>();
        this.dbhelper = dbhelper;
    }


    public void onModelEdited(String t, String c) {
        model.setTitle(t);
        model.setContents(c);
    }

    public void setModel(Todo t) {
        this.model = t;
        //model.setTitle(t.getTitle());
        view.setModel(t);
    }

    public Todo getModel() { return model; }

    public void save() {
        if(listeners.isEmpty()) {
            // if there are no listeners (no shared presenter)
            // then send the text back to the main activity
            view.sendToMainActivity(model.getTitle());
        } else {
            // otherwise, clear the edit text and emit the SAVE event
            view.clear();
            dbhelper.updateTodo(model);
            for (IEventListener listener : listeners) {
                listener.onEvent(new Event(Events.SAVE, model));
            }
        }
    }

    public void subscribe(IEventListener event) {
        listeners.add(event);
    }

    /**
     * The event object that this presenter emits
     */
    private class Event implements IEventEmitter.IEvent<Events> {

        Events type;
        Todo value;
        Event(Events type, Todo value){
            this.type = type;
            this.value = value;
        }

        @Override
        public Events getType() {
            return type;
        }

        @Override
        public Todo getTodo() {
            return value;
        }
    }
}
